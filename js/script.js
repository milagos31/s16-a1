// for (x = 1; x <= 1000; x++) {
//   console.log(x);
// }

// let x = 1;
// while (x <= 1000) {
//   console.log(`count is ${x}`);
//   x++;
// }

console.log("==============1st==============");

let x = -10;
while (x <= 19) {
  console.log(`${x}`);
  x++;
}

console.log("==============2nd==============");

let y = 10;
while (y <= 40) {
  if (y % 2 === 0) {
    console.log(`${y}`);
  }

  y++;
}

console.log("==============3rd==============");

let z = 300;
while (z <= 333) {
  if (z % 2 !== 0) {
    console.log(`${z}`);
  }

  z++;
}
